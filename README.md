# AMETORY FLUTTER FRAMEWORK
This package help you build powerfull flutter pattern

## INSTALLATION
add flutter dependency:

### INSTALL FROM PUB.DEV
```bash
flutter pub add ametory_framework
```
### INSTALL FROM GITLAB


```yaml
dependencies:
  flutter:
    sdk: flutter
  // other package
  ametory_framework:
    git:
      url: git@gitlab.com:ametory-open-source/ametory-flutter-framework.git
      ref: main
```

## Create new flutter project
```bash
flutter create  new_project
```

## Init framework
first of all, you have to init framework to your flutter project
```bash
flutter pub run ametory_framework:init
```
for web compability, please add ```--web``` argument
```bash
flutter pub run ametory_framework:init --web
```

## Generate Feature
first of all, you have to init framework to your flutter project
```bash
flutter pub run ametory_framework:generate ExampleFeature
```

## Add Provider
```bash
flutter pub run ametory_framework:provider chat_bubble
```
## Add Widget
for common widget
```bash
flutter pub run ametory_framework:widget product --data id:int,name:string,is_active:bool,padding:float,onTap:function 
```
or
```bash
flutter pub run ametory_framework:widget product --data id:int,name:string,is_active:bool,padding:float,onTap:function --feature home                         
```
for feature widget

## Add Model
for common model
```bash
flutter pub run ametory_framework:model product --data id:int,name:string,is_active:bool,padding:float,onTap:function 
```
or
```bash
flutter pub run ametory_framework:model product --data id:int,name:string,is_active:bool,padding:float,onTap:function --feature home                         
```
for feature model

## Add Page
```bash
flutter pub run ametory_framework:page product --feature home  
```
## Generate Doc
for first initial doc
```bash
flutter pub run ametory_framework:doc --init
```
or if you have initialized
```bash
flutter pub run ametory_framework:doc
```

## Add Alias
- Edit the ~/.bash_aliases or ~/.bashrc (recommended) file using a text editor:
```bash
vi ~/.bash_aliases
# or #
nano ~/.bashrc
# or #
nano ~/.zshrc
```
- Append your bash alias
For example append:
```bash
alias aff_init='flutter pub run ametory_framework:init'
alias aff_gen='flutter pub run ametory_framework:generate'
alias aff_provider='flutter pub run ametory_framework:provider'
alias aff_model='flutter pub run ametory_framework:model'
alias aff_widget='flutter pub run ametory_framework:widget'
alias aff_page='flutter pub run ametory_framework:page'
alias aff_doc='flutter pub run ametory_framework:doc'
alias aff_admin='flutter pub run ametory_framework:admin'
```
- Save and close the file.
- Activate alias by typing the following source command:

```bash
source ~/.bash_aliases
# or #
source ~/.bashrc
# or #
source ~/.zshrc
```
### use alias
```bash
aff_widget product --data id:int,name:string,is_active:bool,padding:float,onTap:function --feature home                         
```
## Run Flutter
```bash
flutter run -t lib/main_development.dart -d chrome 
```

## ADMIN BOILERPLATE
after init ametory framework, follow this command:

```bash
flutter pub run ametory_framework:admin --feature user,company                        
```

or add new feature 
```bash
flutter pub run ametory_framework:admin --add invoice                     
```


## What Next
- [x] Admin Generator
- [x] Dashboard Template
- [x] Login Template
- [x] Register Template
- [ ] Page Generator Template

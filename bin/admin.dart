// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:recase/recase.dart';

import 'generate.dart' as generate;
import 'provider.dart' as provider;
import 'package:json2yaml/json2yaml.dart';
import 'package:path/path.dart' as path;
import 'package:yaml/yaml.dart';

Future<void> main(List<String> args) async {
  var location = Platform.script.toString();
  var isNewFlutter = location.contains(".snapshot");
  if (isNewFlutter) {
    var sp = Platform.script.toFilePath();
    var sd = sp.split(Platform.pathSeparator);
    sd.removeLast();
    var scriptDir = sd.join(Platform.pathSeparator);
    var packageConfigPath = [scriptDir, '..', '..', '..', 'package_config.json']
        .join(Platform.pathSeparator);
    // print(packageConfigPath);
    var jsonString = File(packageConfigPath).readAsStringSync();
    // print(jsonString);
    Map<String, dynamic> packages = jsonDecode(jsonString);
    var packageList = packages["packages"];
    String? ametoryUri;
    for (var package in packageList) {
      if (package["name"] == "ametory_framework") {
        ametoryUri = package["rootUri"];
        break;
      }
    }
    if (ametoryUri == null) {
      print("error uri");
      return;
    }
    if (ametoryUri.contains("../../")) {
      ametoryUri = ametoryUri.replaceFirst("../", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    if (ametoryUri.contains("file:///")) {
      ametoryUri = ametoryUri.replaceFirst("file://", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    location = ametoryUri;
  }

  if (!args.contains("--feature") && !args.contains("--feature")) {
    print(
        "please add --feature {a,b,c}\nor --add {a,b,c}\nex: --feature user,company,subscribe\nor\n--add user,company,subscribe");
  }
  String appRootFolder = path.absolute("", "");
  String libPath = "$location/lib";

  if (args.contains("--feature")) {
    var pubFile = File("$appRootFolder/pubspec.yaml");
    var doc = loadYaml(pubFile.readAsStringSync(), recover: true);
    var pubSpec = jsonDecode(jsonEncode(doc));
    pubSpec["dependencies"]["syncfusion_flutter_charts"] = "";

    pubFile.writeAsStringSync(json2yaml(pubSpec));
    print("INSTALL DEPENDENCY ....");
    Process.run("flutter", ["pub", "get"]);

    await provider.main(["user_data", "--user"]);
    File("$location/assets/images/icons/logo.png")
        .copySync("$appRootFolder/assets/images/icons/logo.png");
    // CREATE MAIN LAYOUT
    File("$libPath/template/admin/main_layout.dart.stub")
        .copySync("$appRootFolder/lib/common/main_layout.dart");

    List<String> features = ["dashboard"];
    features.addAll(args[args.indexOf("--feature") + 1].split(","));
    features.add("login");
    features.add("profile");
    features.add("setting");
    var menuListContent =
        await generateFeature(features, libPath, appRootFolder, 0);

    // GENERATE MAIN MENU
    final mainMenuContent =
        File("$libPath/template/admin/main_menu_widget.dart.stub")
            .readAsStringSync()
            .replaceAll("      // LIST MENU",
                "$menuListContent\n      // LIST ADD MENU");

    File("$appRootFolder/lib/common/main_menu_widget.dart")
        .writeAsStringSync(mainMenuContent);

    File("$appRootFolder/lib/feature/login/ui/screen/login_screen.dart")
        .writeAsStringSync(
            File("$libPath/template/admin/login_screen.dart.stub")
                .readAsStringSync());

    File("$appRootFolder/lib/feature/dashboard/ui/screen/dashboard_screen.dart")
        .writeAsStringSync(
            File("$libPath/template/admin/dashboard_screen.dart.stub")
                .readAsStringSync());
  }

  if (args.contains("--add")) {
    int lastIndex = 0;
    if (args.contains("--index")) {
      lastIndex = int.parse(args[args.indexOf("--index") + 1]);
    }

    List<String> features = args[args.indexOf("--add") + 1].split(",");
    var menuListContent =
        await generateFeature(features, libPath, appRootFolder, lastIndex);
    final addMenuContent =
        File("$appRootFolder/lib/common/main_menu_widget.dart")
            .readAsStringSync()
            .replaceAll("      // LIST ADD MENU",
                "$menuListContent      // LIST ADD MENU");
    File("$appRootFolder/lib/common/main_menu_widget.dart")
        .writeAsStringSync(addMenuContent);
  }
}

Future<String> generateFeature(List<String> features, String libPath,
    String appRootFolder, int lastIndex) async {
  var menuListContent = "";
  int index = 0;
  for (var e in features) {
    ReCase rc = ReCase(e);
    if (e != "login" && e != "profile" && e != "setting") {
      menuListContent +=
          File("$libPath/template/admin/main_menu_item.dart.stub")
              .readAsStringSync()
              .replaceAll("@index", (index + lastIndex).toString())
              .replaceAll("example", rc.snakeCase)
              .replaceAll("Example", rc.pascalCase)
              .replaceAll("EXAMPLE", rc.constantCase);
      menuListContent += "\n";
    }

    String screenContent = File("$libPath/template/admin/screen.dart.stub")
        .readAsStringSync()
        .replaceAll("@index", (index + lastIndex).toString())
        .replaceAll("example", rc.snakeCase)
        .replaceAll("Example", rc.pascalCase)
        .replaceAll("EXAMPLE", rc.constantCase);
    await generate.main([e]);
    File("$appRootFolder/lib/feature/${rc.snakeCase}/ui/screen/${rc.snakeCase}_screen.dart")
        .writeAsStringSync(screenContent);
    index++;
  }

  return menuListContent;
}

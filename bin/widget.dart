// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:io';
import 'package:ametory_framework/helper/utils.dart';
import 'package:path/path.dart' as path;
import 'package:recase/recase.dart';
import 'package:yaml/yaml.dart';

void main(List<String> args) async {
  var location = Platform.script.toString();
  var isNewFlutter = location.contains(".snapshot");
  if (isNewFlutter) {
    var sp = Platform.script.toFilePath();
    var sd = sp.split(Platform.pathSeparator);
    sd.removeLast();
    var scriptDir = sd.join(Platform.pathSeparator);
    var packageConfigPath = [scriptDir, '..', '..', '..', 'package_config.json']
        .join(Platform.pathSeparator);
    // print(packageConfigPath);
    var jsonString = File(packageConfigPath).readAsStringSync();
    // print(jsonString);
    Map<String, dynamic> packages = jsonDecode(jsonString);
    var packageList = packages["packages"];
    String? ametoryUri;
    for (var package in packageList) {
      if (package["name"] == "ametory_framework") {
        ametoryUri = package["rootUri"];
        break;
      }
    }
    if (ametoryUri == null) {
      print("error uri");
      return;
    }
    if (ametoryUri.contains("../../")) {
      ametoryUri = ametoryUri.replaceFirst("../", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    if (ametoryUri.contains("file:///")) {
      ametoryUri = ametoryUri.replaceFirst("file://", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    location = ametoryUri;
  }

  if (args.isEmpty) {
    printError("Please named widget first first");
    return;
  }
  String libPath = "$location/lib";
  String othersPath = "$libPath/template/others";

  // Get exampleModel data
  ReCase rc = ReCase(args.first);
  String appRootFolder = path.absolute("", "");
  String widgetContent =
      File("$othersPath/widget.dart.stub").readAsStringSync();
  widgetContent = widgetContent.replaceAll("Example", rc.pascalCase);
  if (!args.contains("--data")) {
    printError(
        "Please define --data\nex: --data id:int,name:string,is_active:bool");
    return;
  }
  // CREATE MODEL PARAMS
  if (args.length < 3) {
    printError(
        "Please define --data\nex: --data id:int,name:string,is_active:bool");
    return;
  }

  // print(widgetContent);
  // return;
  // args.indexOf("--data");
  final data = args[args.indexOf("--data") + 1];
  final splitData = data.split(",");
  // split by comma separate

  var widgetParams = "";
  var widgetConstructorParams = "";
  for (var e in splitData) {
    if (e.split(":").length < 2) {
      printError(
          "Please define --data\nex: --data id:int,name:string,is_active:bool");
      return;
    }
    String key = e.split(":")[0];
    ReCase rcKey = ReCase(key);
    final type = e.split(":")[1];
    widgetParams += "    final ${stringToType(type)}? ${rcKey.camelCase};\n";
    widgetConstructorParams += "        this.${rcKey.camelCase},\n";
  }
  widgetContent =
      widgetContent.replaceAll("    // WIDGET PARAMS", widgetParams);
  widgetContent = widgetContent.replaceAll(
      "        // WIDGET CONSTRUCTOR", widgetConstructorParams);

  bool isFeature = false;
  if (args.contains("--feature")) {
    isFeature = true;
  }

  String widgetPath =
      "$appRootFolder/lib/common/widgets/${rc.snakeCase}_widget.dart";
  if (!Directory("$appRootFolder/lib/common/widgets").existsSync()) {
    print("CREATE FOLDER => $appRootFolder/lib/common/widgets");
    Directory("$appRootFolder/lib/common/widgets").createSync(recursive: true);
  }
  if (isFeature) {
    final feature = args[args.indexOf("--feature") + 1];
    ReCase rcFeature = ReCase(feature);
    if (!Directory(
            "$appRootFolder/lib/feature/${rcFeature.snakeCase}/ui/widget")
        .existsSync()) {
      print(
          "CREATE FOLDER => $appRootFolder/lib/feature/${rcFeature.snakeCase}/ui/widget");
      Directory("$appRootFolder/lib/feature/${rcFeature.snakeCase}/ui/widget")
          .createSync(recursive: true);
    }
    widgetPath =
        "$appRootFolder/lib/feature/${rcFeature.snakeCase}/ui/widget/${rc.snakeCase}_widget.dart";
  }

  File(widgetPath).writeAsStringSync(widgetContent);

  print("COMPLETED WRITE WIDGET TO $widgetPath ...");
}

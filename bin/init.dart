// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:io';

import 'package:json2yaml/json2yaml.dart';
import 'package:path/path.dart' as path;
import 'package:yaml/yaml.dart';

import 'generate.dart' as generate;
import 'admin.dart' as admin;

Future<void> main(List<String> args) async {
  var location = Platform.script.toString();
  var isNewFlutter = location.contains(".snapshot");
  if (isNewFlutter) {
    var sp = Platform.script.toFilePath();
    var sd = sp.split(Platform.pathSeparator);
    sd.removeLast();
    var scriptDir = sd.join(Platform.pathSeparator);
    var packageConfigPath = [scriptDir, '..', '..', '..', 'package_config.json'].join(Platform.pathSeparator);
    // print(packageConfigPath);
    var jsonString = File(packageConfigPath).readAsStringSync();
    // print(jsonString);
    Map<String, dynamic> packages = jsonDecode(jsonString);
    var packageList = packages["packages"];
    String? ametoryUri;
    for (var package in packageList) {
      if (package["name"] == "ametory_framework") {
        ametoryUri = package["rootUri"];
        break;
      }
    }
    if (ametoryUri == null) {
      print("error uri");
      return;
    }
    if (ametoryUri.contains("../../")) {
      ametoryUri = ametoryUri.replaceFirst("../", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    if (ametoryUri.contains("file:///")) {
      ametoryUri = ametoryUri.replaceFirst("file://", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    location = ametoryUri;
  }

  String appRootFolder = path.absolute("", "");
  var pubFile = File("$appRootFolder/pubspec.yaml");
  var doc = loadYaml(pubFile.readAsStringSync(), recover: true);
  bool isWeb = false;
  String appName = doc['name'];
  String libPath = "$location/lib";
  String commonPath = "$libPath/template/common";
  String othersPath = "$libPath/template/others";
  String commonAppPath = "$appRootFolder/lib/common";
  String routerAppPath = "$commonAppPath/route";
  String libAppPath = "$appRootFolder/lib";
  var pubSpec = jsonDecode(jsonEncode(doc));
  for (var e in packages) {
    pubSpec["dependencies"][e.name] = e.version;
  }

  if (args.contains("--web")) {
    isWeb = true;
  }

  pubSpec["is_web"] = isWeb;
  pubSpec["dependency_overrides"] = {
    "firebase_core_platform_interface": "4.5.1",
    "webview_flutter": "^2.0.12",
  };
  if (isWeb) {
    pubSpec["dependencies"]["fluro"] = "";
    pubSpec["dependencies"]["url_launcher"] = "";
    pubSpec["dependencies"]["dart_markdown"] = "";
    pubSpec["dependencies"]["html2md"] = "";
    pubSpec["dependencies"]["scroll_to_id"] = "";
    pubSpec["dependencies"]["collection"] = "";
  }
  if (!pubSpec["flutter"].containsKey("assets")) {
    pubSpec["flutter"]["assets"] = [];
  }
  if (!pubSpec["flutter"]["assets"].contains("assets/images/")) {
    pubSpec["flutter"]["assets"].add("assets/images/");
  }
  if (!pubSpec["flutter"]["assets"].contains("assets/images/icons/")) {
    pubSpec["flutter"]["assets"].add("assets/images/icons/");
  }
  if (!pubSpec["flutter"]["assets"].contains("assets/docs/")) {
    pubSpec["flutter"]["assets"].add("assets/docs/");
  }

  if (!Directory("$appRootFolder/assets/images").existsSync()) {
    print("CREATE FOLDER => $appRootFolder/assets/images");
    Directory("$appRootFolder/assets/images").createSync(recursive: true);
  }
  if (!Directory("$appRootFolder/assets/docs").existsSync()) {
    print("CREATE FOLDER => $appRootFolder/assets/docs");
    Directory("$appRootFolder/assets/docs").createSync(recursive: true);
  }
  if (!Directory("$appRootFolder/assets/images/icons").existsSync()) {
    print("CREATE FOLDER => $appRootFolder/assets/images/icons");
    Directory("$appRootFolder/assets/images/icons").createSync(recursive: true);
  }

  pubFile.writeAsStringSync(json2yaml(pubSpec));

  print("INSTALL DEPENDENCY ....");
  Process.run("flutter", ["pub", "get"]);

  // ADD GRPC CLIENT
  String grpcContent = File("$commonPath/constants/grpc_client.dart.stub").readAsStringSync();
  String grpcFile = "$commonAppPath/constants/grpc_client.dart";
  if (!Directory("$commonAppPath/constants").existsSync()) {
    print("CREATE FOLDER => $commonAppPath/constants");
    Directory("$commonAppPath/constants").createSync(recursive: true);
  }
  print("Create File GRPC CLIENT $grpcFile");
  File(grpcFile).writeAsStringSync(grpcContent);

  // ADD ENV
  String envContent = File("$commonPath/constants/env.dart.stub").readAsStringSync();
  String envFile = "$commonAppPath/constants/env.dart";
  if (!Directory("$commonAppPath/constants").existsSync()) {
    print("CREATE FOLDER => $commonAppPath/constants");
    Directory("$commonAppPath/constants").createSync(recursive: true);
  }
  print("Create File ENV CLIENT $grpcFile");
  File(envFile).writeAsStringSync(envContent);

  // // ADD BLOC DELEGATE
  // String blocDelegateContent =
  //     File("$commonPath/bloc/simple_bloc_delegate.dart.stub")
  //         .readAsStringSync();
  // String blocDelegateFile = "$commonAppPath/bloc/simple_bloc_delegate.dart";
  // if (!Directory("$commonAppPath/bloc").existsSync()) {
  //   print("CREATE FOLDER => $commonAppPath/bloc");
  //   Directory("$commonAppPath/bloc").createSync(recursive: true);
  // }
  // print("Create File BLOC DELEGATE $grpcFile");
  // File(blocDelegateFile).writeAsStringSync(blocDelegateContent);

  // ADD MAIN DEVELOPMENT
  String mainContent = File("$othersPath/main.dart.stub").readAsStringSync();
  String mainDevelopmentFile = "$libAppPath/main_development.dart";
  String mainStagingFile = "$libAppPath/main_staging.dart";
  String mainProductionFile = "$libAppPath/main_production.dart";
  print("Create File Main Development $mainDevelopmentFile");
  File(mainDevelopmentFile).writeAsStringSync(mainContent.replaceAll("appName", appName));
  print("Create File Main Staging $mainDevelopmentFile");
  File(mainStagingFile).writeAsStringSync(mainContent.replaceAll("appName", appName).replaceAll("development", "staging"));
  print("Create File Main Production $mainDevelopmentFile");
  File(mainProductionFile).writeAsStringSync(mainContent.replaceAll("appName", appName).replaceAll("development", "production"));

  // ADD APP
  String appContent = File(isWeb ? "$othersPath/app_web.dart.stub" : "$othersPath/app.dart.stub").readAsStringSync();
  String appFile = "$libAppPath/app/app.dart";
  if (!Directory("$libAppPath/app").existsSync()) {
    print("CREATE FOLDER => $libAppPath/app");
    Directory("$libAppPath/app").createSync(recursive: true);
  }
  print("Create File APP $mainDevelopmentFile");
  File(appFile).writeAsStringSync(appContent.replaceAll("appName", appName));

  // ADD ROUTER

  if (!Directory(routerAppPath).existsSync()) {
    print("CREATE FOLDER => $routerAppPath");
    Directory(routerAppPath).createSync(recursive: true);
  }

  if (isWeb) {
    String routerContent = File("$commonPath/route/router_fluro.dart.stub").readAsStringSync();
    String routerFile = "$routerAppPath/router.dart";
    File(routerFile).writeAsStringSync(routerContent.replaceAll("appName", appName));
    String routerHandlerContent = File("$commonPath/route/router_handler_fluro.dart.stub").readAsStringSync();
    String routerHandlerFile = "$routerAppPath/router_handler.dart";
    File(routerHandlerFile).writeAsStringSync(routerHandlerContent.replaceAll("appName", appName));
  } else {
    String routerContent = File("$commonPath/route/router.dart.stub").readAsStringSync();
    String routerFile = "$routerAppPath/router.dart";
    File(routerFile).writeAsStringSync(routerContent);
    String routerGeneratorContent = File("$commonPath/route/router_generator.dart.stub").readAsStringSync();
    String routerGeneratorFile = "$routerAppPath/router_generator.dart";
    File(routerGeneratorFile).writeAsStringSync(routerGeneratorContent.replaceAll("appName", appName));
  }

  // ADD DARK THEME
  String darkThemeProviderContent = File("$commonPath/styles/dark_theme_provider.dart.stub").readAsStringSync();
  String darkThemeProviderFile = "$commonAppPath/styles/dark_theme_provider.dart";
  if (!Directory("$commonAppPath/styles").existsSync()) {
    print("CREATE FOLDER => $commonAppPath/styles");
    Directory("$commonAppPath/styles").createSync(recursive: true);
  }
  print("Create File DARK THEME PROVIDER $darkThemeProviderFile");
  File(darkThemeProviderFile).writeAsStringSync(darkThemeProviderContent.replaceAll("appName", appName));
  String darkThemePreferenceContent = File("$commonPath/styles/dark_theme_preference.dart.stub").readAsStringSync();
  String darkThemePreferenceFile = "$commonAppPath/styles/dark_theme_preference.dart";
  print("Create File DARK THEME PREFERENCE $darkThemePreferenceFile");
  File(darkThemePreferenceFile).writeAsStringSync(darkThemePreferenceContent);

  print("add home feature ....");
  await generate.main(["home"]);

  if (args.contains("--blog")) {
    // COPY HOME SCREEN

    File("$libAppPath/feature/home/ui/screen/home_screen.dart").writeAsStringSync(File("$libPath/template/blog/home_screen.dart.stub").readAsStringSync());
    await admin.main(["--feature", "blog", "user"]);
    await generate.main(["category"]);
    await generate.main(["tag"]);
    File("$libAppPath/feature/blog/ui/screen/blog_detail_screen.dart")
        .writeAsStringSync(File("$libPath/template/blog/blog_detail_screen.dart.stub").readAsStringSync());
  }
  // Process.run("flutter", [
  //   "pub",
  //   "run",
  //   "ametory_framework:generate",
  //   "home",
  //   isWeb ? "--web" : ""
  // ]);
  print("add splash feature ....");
  await generate.main(["splash"]);
  // await Future.delayed(const Duration(seconds: 3));
  // Process.run("flutter", [
  //   "pub",
  //   "run",
  //   "ametory_framework:generate",
  //   "splash",
  //   isWeb ? "--web" : ""
  // ]);

  print("completed ....");
}

class Package {
  String name;
  String version;

  Package(this.name, this.version);
}

List<Package> packages = [
  Package("flutter_bloc", ""),
  Package("bloc", ""),
  Package("page_transition", ""),
  Package("timeago", ""),
  Package("timezone", ""),
  Package("equatable", ""),
  Package("grpc", ""),
  Package("intl", ""),
  Package("provider", ""),
  Package("shared_preferences", ""),
  Package("url_strategy", ""),
];

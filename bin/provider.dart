// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:io';

import 'package:ametory_framework/helper/utils.dart';
import 'package:path/path.dart' as path;
import 'package:recase/recase.dart';
import 'package:yaml/yaml.dart';

Future<void> main(List<String> args) async {
  var location = Platform.script.toString();
  var isNewFlutter = location.contains(".snapshot");
  if (isNewFlutter) {
    var sp = Platform.script.toFilePath();
    var sd = sp.split(Platform.pathSeparator);
    sd.removeLast();
    var scriptDir = sd.join(Platform.pathSeparator);
    var packageConfigPath = [scriptDir, '..', '..', '..', 'package_config.json']
        .join(Platform.pathSeparator);
    // print(packageConfigPath);
    var jsonString = File(packageConfigPath).readAsStringSync();
    // print(jsonString);
    Map<String, dynamic> packages = jsonDecode(jsonString);
    var packageList = packages["packages"];
    String? ametoryUri;
    for (var package in packageList) {
      if (package["name"] == "ametory_framework") {
        ametoryUri = package["rootUri"];
        break;
      }
    }
    if (ametoryUri == null) {
      print("error uri");
      return;
    }
    if (ametoryUri.contains("../../")) {
      ametoryUri = ametoryUri.replaceFirst("../", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    if (ametoryUri.contains("file:///")) {
      ametoryUri = ametoryUri.replaceFirst("file://", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    location = ametoryUri;
  }

  String appRootFolder = path.absolute("", "");
  var pubFile = File("$appRootFolder/pubspec.yaml");
  var doc = loadYaml(pubFile.readAsStringSync(), recover: true);
  // bool isWeb = false;
  String appName = doc['name'];
  String libPath = "$location/lib";
  String commonPath = "$libPath/template/common";
  // String othersPath = "$libPath/template/others";
  String commonAppPath = "$appRootFolder/lib/common";
  // String routerAppPath = "$commonAppPath/route";
  // String libAppPath = "$appRootFolder/lib";
  // var pubSpec = jsonDecode(jsonEncode(doc));

  if (args.isEmpty) {
    printError("Please named provider");
    return;
  }

  ReCase rc = ReCase(args.first);

  String appPath = "$appRootFolder/lib/app/app.dart";
  String appPathContent = File(appPath).readAsStringSync();

  if (!Directory("$commonAppPath/providers").existsSync()) {
    print("CREATE FOLDER => $commonAppPath/providers");
    Directory("$commonAppPath/providers").createSync(recursive: true);
  }

  // APPEND APP FILE
  String providerImport =
      "import 'package:appName/common/providers/${rc.snakeCase}_provider.dart';"
          .replaceAll("appName", appName);
  String providerInit =
      """${rc.pascalCase}Provider ${rc.camelCase}Provider = ${rc.pascalCase}Provider();""";
  String providerConfig = """ChangeNotifierProvider<${rc.pascalCase}Provider>(
            create: (_) => ${rc.camelCase}Provider),""";
  if (!appPathContent.contains(providerImport)) {
    appPathContent = appPathContent.replaceFirst(
        "// ADD PROVIDER IMPORT PLEASE DON'T REMOVE THIS LINE",
        "$providerImport\n\n// ADD PROVIDER IMPORT PLEASE DON'T REMOVE THIS LINE");
  }
  if (!appPathContent.contains(providerInit)) {
    appPathContent = appPathContent.replaceFirst(
        "// ADD PROVIDER INIT PLEASE DON'T REMOVE THIS LINE",
        "$providerInit\n  // ADD PROVIDER INIT PLEASE DON'T REMOVE THIS LINE");
  }
  if (!appPathContent.contains(providerConfig)) {
    appPathContent = appPathContent.replaceFirst(
        "// ADD PROVIDER CONFIG PLEASE DON'T REMOVE THIS LINE",
        "$providerConfig\n        // ADD PROVIDER CONFIG PLEASE DON'T REMOVE THIS LINE");
  }
  // print(appPathContent);
  File(appPath).writeAsStringSync(appPathContent);

  // CREATE PROVIDER FILE
  String providerAppFile =
      "$commonAppPath/providers/${rc.snakeCase}_provider.dart";
  String preferenceAppFile =
      "$commonAppPath/providers/${rc.snakeCase}_preference.dart";
  String providerAppContent = "";
  String preferenceAppContent = "";
  if (args.contains("--user")) {
    providerAppContent = File("$commonPath/providers/user_provider.dart.stub")
        .readAsStringSync();
    preferenceAppContent =
        File("$commonPath/providers/user_preference.dart.stub")
            .readAsStringSync();
    providerAppContent = providerAppContent
        .replaceAll("USER", rc.constantCase)
        .replaceAll("user", rc.camelCase)
        .replaceAll("User", rc.pascalCase)
        .replaceAll("exImport", rc.snakeCase)
        .replaceAll("@MODEL", "UserModel");
    File(providerAppFile).writeAsStringSync(providerAppContent);
    preferenceAppContent = preferenceAppContent
        .replaceAll("PROVIDER", rc.constantCase)
        .replaceAll("provider", rc.camelCase)
        .replaceAll("Provider", rc.pascalCase)
        .replaceAll("exImport", rc.snakeCase)
        .replaceAll("@MODEL", "UserModel");
  } else {
    providerAppContent =
        File("$commonPath/providers/example_provider.dart.stub")
            .readAsStringSync();
    preferenceAppContent =
        File("$commonPath/providers/example_preference.dart.stub")
            .readAsStringSync();
    providerAppContent = providerAppContent
        .replaceAll("exImport", rc.snakeCase)
        .replaceAll("EXAMPLE", rc.constantCase)
        .replaceAll("example", rc.camelCase)
        .replaceAll("Example", rc.pascalCase);
    File(providerAppFile).writeAsStringSync(providerAppContent);
    preferenceAppContent = preferenceAppContent
        .replaceAll("exImport", rc.snakeCase)
        .replaceAll("EXAMPLE", rc.constantCase)
        .replaceAll("example", rc.camelCase)
        .replaceAll("Example", rc.pascalCase);
  }

  File(preferenceAppFile).writeAsStringSync(preferenceAppContent);
  print("COMPLETED ...");
}

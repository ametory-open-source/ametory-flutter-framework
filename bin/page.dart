// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:io';
import 'package:ametory_framework/helper/utils.dart';
import 'package:path/path.dart' as path;
import 'package:recase/recase.dart';
import 'package:yaml/yaml.dart';

void main(List<String> args) async {
  var location = Platform.script.toString();
  var isNewFlutter = location.contains(".snapshot");
  if (isNewFlutter) {
    var sp = Platform.script.toFilePath();
    var sd = sp.split(Platform.pathSeparator);
    sd.removeLast();
    var scriptDir = sd.join(Platform.pathSeparator);
    var packageConfigPath = [scriptDir, '..', '..', '..', 'package_config.json']
        .join(Platform.pathSeparator);
    // print(packageConfigPath);
    var jsonString = File(packageConfigPath).readAsStringSync();
    // print(jsonString);
    Map<String, dynamic> packages = jsonDecode(jsonString);
    var packageList = packages["packages"];
    String? ametoryUri;
    for (var package in packageList) {
      if (package["name"] == "ametory_framework") {
        ametoryUri = package["rootUri"];
        break;
      }
    }
    if (ametoryUri == null) {
      print("error uri");
      return;
    }
    if (ametoryUri.contains("../../")) {
      ametoryUri = ametoryUri.replaceFirst("../", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    if (ametoryUri.contains("file:///")) {
      ametoryUri = ametoryUri.replaceFirst("file://", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    location = ametoryUri;
  }

  if (args.isEmpty) {
    printError("Please named widget first first");
    return;
  }
  String libPath = "$location/lib";
  String templatePath = "$libPath/template/feature/example";

  ReCase rc = ReCase(args.first);
  String appRootFolder = path.absolute("", "");
  var pubFile = File("$appRootFolder/pubspec.yaml");
  var doc = loadYaml(pubFile.readAsStringSync());
  bool isWeb = false;
  String appName = doc['name'];

  if (doc.containsKey("is_web")) {
    isWeb = doc['is_web'];
  }

  if (!args.contains("--feature")) {
    printError("Please named feature first");
    return;
  }
  int indexFeature = args.indexOf("--feature") + 1;
  if (args.length <= indexFeature) {
    printError("Please named feature first");
    return;
  }
  final feature = args[indexFeature];
  ReCase rcFeature = ReCase(feature);
  String featureAppPath = "$appRootFolder/lib/feature/${rcFeature.snakeCase}";

  // GET TEMPLATE
  String pageContent =
      File("$templatePath/ui/page/example_page.dart.stub").readAsStringSync();
  pageContent = pageContent.replaceAll("appName", appName);
  pageContent =
      pageContent.replaceAll("ExampleBloc", "${rcFeature.pascalCase}Bloc");
  pageContent = pageContent.replaceAll("Example", rc.pascalCase);
  pageContent = pageContent.replaceAll("example", rcFeature.snakeCase);
  pageContent = pageContent.replaceAll(
      "${rcFeature.snakeCase}_arguments", "${rc.snakeCase}_arguments");
  pageContent = pageContent.replaceAll(
      "${rcFeature.snakeCase}_screen.dart", "${rc.snakeCase}_screen.dart");
  String pageFile = "$featureAppPath/ui/page/${rc.snakeCase}_page.dart";

  String screenContent =
      File("$templatePath/ui/screen/example_screen.dart.stub")
          .readAsStringSync();
  screenContent = screenContent.replaceAll("appName", appName);
  screenContent =
      screenContent.replaceAll("ExampleBloc", "${rcFeature.pascalCase}Bloc");
  screenContent =
      screenContent.replaceAll("ExampleState", "${rcFeature.pascalCase}State");
  screenContent =
      screenContent.replaceAll("ExampleEvent", "${rcFeature.pascalCase}Event");
  screenContent = screenContent.replaceAll("Example", rc.pascalCase);
  screenContent = screenContent.replaceAll("EXAMPLE", rc.constantCase);
  screenContent = screenContent.replaceAll("example", rcFeature.snakeCase);
  screenContent = screenContent.replaceAll(
      "${rcFeature.snakeCase}_arguments", "${rc.snakeCase}_arguments");
  String screenFile = "$featureAppPath/ui/screen/${rc.snakeCase}_screen.dart";

  String argumentsContent = "class ${rc.pascalCase}Arguments {\n}";

  String argumentFile =
      "$featureAppPath/arguments/${rc.snakeCase}_arguments.dart";

  File(pageFile).writeAsStringSync(pageContent);
  File(screenFile).writeAsStringSync(screenContent);
  File(argumentFile).writeAsStringSync(argumentsContent);
  return;
}

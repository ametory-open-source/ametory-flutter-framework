// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:io';

import 'package:ametory_framework/helper/utils.dart';
import 'package:path/path.dart' as path;
import 'package:recase/recase.dart';

void main(List<String> args) async {
  var location = Platform.script.toString();
  var isNewFlutter = location.contains(".snapshot");
  if (isNewFlutter) {
    var sp = Platform.script.toFilePath();
    var sd = sp.split(Platform.pathSeparator);
    sd.removeLast();
    var scriptDir = sd.join(Platform.pathSeparator);
    var packageConfigPath = [scriptDir, '..', '..', '..', 'package_config.json']
        .join(Platform.pathSeparator);
    // print(packageConfigPath);
    var jsonString = File(packageConfigPath).readAsStringSync();
    // print(jsonString);
    Map<String, dynamic> packages = jsonDecode(jsonString);
    var packageList = packages["packages"];
    String? ametoryUri;
    for (var package in packageList) {
      if (package["name"] == "ametory_framework") {
        ametoryUri = package["rootUri"];
        break;
      }
    }
    if (ametoryUri == null) {
      print("error uri");
      return;
    }
    if (ametoryUri.contains("../../")) {
      ametoryUri = ametoryUri.replaceFirst("../", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    if (ametoryUri.contains("file:///")) {
      ametoryUri = ametoryUri.replaceFirst("file://", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    location = ametoryUri;
  }

  if (args.isEmpty) {
    printError("Please named model first");
    return;
  }
  String libPath = "$location/lib";
  String othersPath = "$libPath/template/others";

  // Get exampleModel data
  ReCase rc = ReCase(args.first);
  String appRootFolder = path.absolute("", "");
  String modelContent = File("$othersPath/model.dart.stub").readAsStringSync();
  modelContent = modelContent.replaceAll("Example", rc.pascalCase);
  if (!args.contains("--data")) {
    printError(
        "Please define --data\nex: --data id:int,name:string,is_active:bool");
    return;
  }
  // CREATE MODEL PARAMS
  if (args.length < 3) {
    printError(
        "Please define --data\nex: --data id:int,name:string,is_active:bool");
    return;
  }
  // args.indexOf("--data");
  final data = args[args.indexOf("--data") + 1];
  final splitData = data.split(",");
  // split by comma separate

  var modelParams = "";
  var modelConstructorParams = "";
  var modelFromJson = "";
  var modelToJson = "";
  var modelCopyWithParams = "";
  var modelCopyWithData = "";
  for (var e in splitData) {
    if (e.split(":").length < 2) {
      printError(
          "Please define --data\nex: --data id:int,name:string,is_active:bool");
      return;
    }
    String key = e.split(":")[0];
    ReCase rcKey = ReCase(key);
    final type = e.split(":")[1];
    modelParams += "  ${stringToType(type)}? ${rcKey.camelCase};\n";
    modelConstructorParams += "    this.${rcKey.camelCase},\n";
    modelFromJson += "    ${rcKey.camelCase} = json['${rcKey.snakeCase}'];\n";
    modelToJson += "    data['${rcKey.snakeCase}'] = ${rcKey.camelCase};\n";
    modelCopyWithParams += "  ${stringToType(type)}? ${rcKey.camelCase},\n";
    modelCopyWithData +=
        "      ${rcKey.camelCase}: ${rcKey.camelCase} ?? this.${rcKey.camelCase},\n";
  }
  modelContent = modelContent.replaceAll("  // MODEL PARAMS", modelParams);
  modelContent = modelContent.replaceAll(
      "    // MODEL CONSTRUCTOR PARAMS", modelConstructorParams);
  modelContent =
      modelContent.replaceAll("    // MODEL FROM JSON DATA", modelFromJson);
  modelContent =
      modelContent.replaceAll("    // MODEL TO JSON DATA", modelToJson);
  modelContent = modelContent.replaceAll(
      "    // MODEL COPY WITH PARAMS", modelCopyWithParams);
  modelContent = modelContent.replaceAll(
      "      // MODEL DATA COPY WITH", modelCopyWithData);

  bool isFeature = false;
  if (args.contains("--feature")) {
    isFeature = true;
  }

  String modelPath =
      "$appRootFolder/lib/common/models/${rc.snakeCase}_model.dart";
  if (!Directory("$appRootFolder/lib/common/models").existsSync()) {
    print("CREATE FOLDER => $appRootFolder/lib/common/models");
    Directory("$appRootFolder/lib/common/models").createSync(recursive: true);
  }
  if (isFeature) {
    final feature = args[args.indexOf("--feature") + 1];
    ReCase rcFeature = ReCase(feature);
    if (!Directory("$appRootFolder/lib/feature/${rcFeature.snakeCase}/models")
        .existsSync()) {
      print(
          "CREATE FOLDER => $appRootFolder/lib/feature/${rcFeature.snakeCase}/models");
      Directory("$appRootFolder/lib/feature/${rcFeature.snakeCase}/models")
          .createSync(recursive: true);
    }
    modelPath =
        "$appRootFolder/lib/feature/${rcFeature.snakeCase}/models/${rc.snakeCase}_model.dart";
  }

  File(modelPath).writeAsStringSync(modelContent);

  print("COMPLETED WRITE MODEL TO $modelPath ...");
}

// ignore_for_file: avoid_print
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ametory_framework/helper/utils.dart';
import 'package:recase/recase.dart';
import 'package:path/path.dart' as path;
import 'package:yaml/yaml.dart';

Future<void> main(List<String> args) async {
  var location = Platform.script.toString();
  var isNewFlutter = location.contains(".snapshot");
  if (isNewFlutter) {
    var sp = Platform.script.toFilePath();
    var sd = sp.split(Platform.pathSeparator);
    sd.removeLast();
    var scriptDir = sd.join(Platform.pathSeparator);
    var packageConfigPath = [scriptDir, '..', '..', '..', 'package_config.json'].join(Platform.pathSeparator);
    // print(packageConfigPath);
    var jsonString = File(packageConfigPath).readAsStringSync();
    // print(jsonString);
    Map<String, dynamic> packages = jsonDecode(jsonString);
    var packageList = packages["packages"];
    String? ametoryUri;
    for (var package in packageList) {
      if (package["name"] == "ametory_framework") {
        ametoryUri = package["rootUri"];
        break;
      }
    }
    if (ametoryUri == null) {
      print("error uri");
      return;
    }
    if (ametoryUri.contains("../../")) {
      ametoryUri = ametoryUri.replaceFirst("../", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    if (ametoryUri.contains("file:///")) {
      ametoryUri = ametoryUri.replaceFirst("file://", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    location = ametoryUri;
  }

  if (args.isEmpty) {
    printError("Please named new feature");
    return;
  }
  ReCase rc = ReCase(args.first);
  String appRootFolder = path.absolute("", "");

  // print(rc.camelCase);
  // print(rc.constantCase);
  // print(rc.snakeCase);
  // print(rc.pascalCase);
  // print(rc.paramCase);
  var pubFile = File("$appRootFolder/pubspec.yaml");
  var doc = loadYaml(pubFile.readAsStringSync());
  bool isWeb = false;
  String appName = doc['name'];

  if (doc.containsKey("is_web")) {
    isWeb = doc['is_web'];
  }
  String libPath = "$location/lib";
  String templatePath = "$libPath/template/feature/example";
  String featureAppPath = "$appRootFolder/lib/feature/${rc.snakeCase}";
  // String commonPath = "$libPath/template/common";
  String commonAppPath = "$appRootFolder/lib/common";
  String routerAppPath = "$commonAppPath/route";
  var list = await dirContents(Directory(templatePath));
  for (var element in list) {
    // CREATE DIRECTORY
    if (element.toString().contains("Directory")) {
      var newFolder = "$featureAppPath${element.path.replaceFirst(templatePath, "")}";
      if (!Directory(newFolder).existsSync()) {
        print("CREATE FOLDER => $newFolder");
        Directory(newFolder).createSync(recursive: true);
      }
    } else {
      String fileContent = await createFile(element.path, rc, appName);
      var newFile = "$featureAppPath${element.path.replaceFirst(templatePath, "").replaceAll("example", rc.snakeCase).replaceFirst(".stub", "")}";
      print("CREATE FILE => $newFile");
      File(newFile).writeAsStringSync(fileContent);

      // break;
    }
  }

  File("$featureAppPath/ui/screen/${rc.snakeCase}_doc_screen.dart").deleteSync();

  // APPEND ROUTER FILE
  print("CREATE MODEL ...");
  Process.run("flutter", [
    "pub",
    "run",
    "ametory_framework:model",
    rc.snakeCase,
    "--data",
    "id:string,name:string,description:string",
    "--feature",
    rc.snakeCase,
  ]);
  if (isWeb) {
    String routerFluroFile = "$routerAppPath/router.dart";
    String routerFluroAppendContent = "static const String ${rc.camelCase}Detail = '/${rc.snakeCase}/:id';";
    routerFluroAppendContent += "\n  static const String ${rc.camelCase} = '/${rc.snakeCase}';";
    routerFluroAppendContent += "\n  static const String ${rc.camelCase}Detail = '/${rc.snakeCase}Detail';";
    if (rc.camelCase == "home") {
      routerFluroAppendContent = "static const String ${rc.camelCase} = '/';";
    }
    String routerHandlerFluroAppendContent = """
router.define(${rc.snakeCase}Detail,
        handler: ${rc.snakeCase}DetailHandler, transitionType: TransitionType.fadeIn);
    router.define(${rc.snakeCase},
        handler: ${rc.snakeCase}Handler, transitionType: TransitionType.fadeIn);
""";
    if (rc.camelCase == "home") {
      routerHandlerFluroAppendContent = """
router.define(${rc.snakeCase},
        handler: ${rc.snakeCase}Handler, transitionType: TransitionType.fadeIn);
""";
    }
    String routerFluroFileContent = File(routerFluroFile).readAsStringSync();
    String newFileRouterFluroContent = routerFluroFileContent
        .replaceAll("// ADD ROUTER PLEASE DON'T REMOVE THIS LINE", "$routerFluroAppendContent\n  // ADD ROUTER PLEASE DON'T REMOVE THIS LINE")
        .replaceAll("// ADD HANDLER PLEASE DON'T REMOVE THIS LINE", "$routerHandlerFluroAppendContent\n    // ADD HANDLER PLEASE DON'T REMOVE THIS LINE");
    if (!File(routerFluroFile).readAsStringSync().contains(routerHandlerFluroAppendContent)) {
      File(routerFluroFile).writeAsStringSync(newFileRouterFluroContent);
    }

    // HANDLER

    String routerHandlerAppendContent = """
Handler? exampleHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return ExamplePage(args: ExampleArguments(params: params));
});

    Handler? exampleDetailHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return ExampleDetailPage(args: ExampleDetailArguments(params: params));
});
  """;
    if (rc.camelCase == "home") {
      routerHandlerAppendContent = """
Handler? exampleHandler = Handler(
    handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
  return ExamplePage(args: ExampleArguments(params: params));
});
""";
    }
    routerHandlerAppendContent = routerHandlerAppendContent.replaceAll("example", rc.camelCase).replaceAll("Example", rc.pascalCase);

    String routerHandlerFile = "$routerAppPath/router_handler.dart";
    String routerHandlerFileContent = File(routerHandlerFile).readAsStringSync();
    String routeImportArgs = "import 'package:$appName/feature/${rc.snakeCase}/arguments/${rc.snakeCase}_arguments.dart';";
    routeImportArgs += "\nimport 'package:$appName/feature/${rc.snakeCase}/arguments/${rc.snakeCase}_detail_arguments.dart';";
    String routeImportPage = "import 'package:$appName/feature/${rc.snakeCase}/ui/page/${rc.snakeCase}_page.dart';";
    routeImportPage += "\nimport 'package:$appName/feature/${rc.snakeCase}/ui/page/${rc.snakeCase}_detail_page.dart';";
    String newFileRouterHandlerContent = routerHandlerFileContent.replaceAll(
        "// ADD ROUTE IMPORT PLEASE DON'T REMOVE THIS LINE", "$routeImportPage\n$routeImportArgs\n// ADD ROUTE IMPORT PLEASE DON'T REMOVE THIS LINE");
    newFileRouterHandlerContent = newFileRouterHandlerContent.replaceFirst(
        "// ADD ROUTE CONFIG PLEASE DON'T REMOVE THIS LINE", "$routerHandlerAppendContent\n// ADD ROUTE CONFIG PLEASE DON'T REMOVE THIS LINE");
    if (!File(routerHandlerFile).readAsStringSync().contains("${rc.camelCase}Handler")) {
      File(routerHandlerFile).writeAsStringSync(newFileRouterHandlerContent);
    }
    return;
  }

  String routerAppendContent = "static const String ${rc.camelCase} = '/${rc.snakeCase}';";
  routerAppendContent += "\n  static const String ${rc.camelCase}Detail = '/${rc.snakeCase}Detail';";
  // File("$commonPath/route/router.dart.stub").readAsStringSync();
  String routerFile = "$routerAppPath/router.dart";
  String routerFileContent = File(routerFile).readAsStringSync();
  String newFileRouterContent = routerFileContent.replaceAll("// PLEASE DON'T REMOVE THIS LINE", "$routerAppendContent\n  // PLEASE DON'T REMOVE THIS LINE");
  String routerGeneratorAppendContent = """
case Routes.example:
        if (args is ExampleArguments) {
          return PageTransition(
              isIos: Platform.isIOS,
              type: PageTransitionType.rightToLeft,
              child: ExamplePage(
                args: args,
              ));
        }
        return _errorRoute();
      case Routes.exampleDetail:
        if (args is ExampleDetailArguments) {
          return PageTransition(
              isIos: Platform.isIOS,
              type: PageTransitionType.rightToLeft,
              child: ExampleDetailPage(
                args: args,
              ));
        }
        return _errorRoute();
  """;
  routerGeneratorAppendContent = routerGeneratorAppendContent.replaceAll("example", rc.camelCase).replaceAll("Example", rc.pascalCase);
  String routerGeneratorFile = "$routerAppPath/router_generator.dart";
  String routerGeneratorFileContent = File(routerGeneratorFile).readAsStringSync();

  String routeImportArgs = "import 'package:$appName/feature/${rc.snakeCase}/arguments/${rc.snakeCase}_arguments.dart';";
  routeImportArgs += "\nimport 'package:$appName/feature/${rc.snakeCase}/arguments/${rc.snakeCase}_detail_arguments.dart';";
  String routeImportPage = "import 'package:$appName/feature/${rc.snakeCase}/ui/page/${rc.snakeCase}_page.dart';";
  routeImportPage += "\nimport 'package:$appName/feature/${rc.snakeCase}/ui/page/${rc.snakeCase}_detail_page.dart';";
  String newFileRouterGeneratorContent =
      routerGeneratorFileContent.replaceAll("// PLEASE DON'T REMOVE THIS LINE", "$routerGeneratorAppendContent\n      // PLEASE DON'T REMOVE THIS LINE");
  newFileRouterGeneratorContent = newFileRouterGeneratorContent.replaceFirst(
      "// PLEASE DON'T REMOVE THIS IMPORT LINE", "$routeImportArgs\n$routeImportPage\n// PLEASE DON'T REMOVE THIS IMPORT LINE");
  File(routerFile).writeAsStringSync(newFileRouterContent);
  File(routerGeneratorFile).writeAsStringSync(newFileRouterGeneratorContent);
}

Future<List<FileSystemEntity>> dirContents(Directory dir) {
  var files = <FileSystemEntity>[];
  var completer = Completer<List<FileSystemEntity>>();
  var lister = dir.list(recursive: true);
  lister.listen((file) => files.add(file),
      // should also register onError
      onDone: () => completer.complete(files));
  return completer.future;
}

Future<String> createFile(String path, ReCase rc, String appName) async {
  // snake_case
// dot.case
// path/case
// param-case
// PascalCase
// Header-Case
// Title Case
// camelCase
// Sentence case
// CONSTANT_CASE
  String result = File(path).readAsStringSync();
  result = result.replaceAll("appName", appName);
  result = result.replaceAll("Example", rc.pascalCase);
  result = result.replaceAll("example", rc.snakeCase);
  result = result.replaceAll("EXAMPLE", rc.constantCase);
  return result;
}

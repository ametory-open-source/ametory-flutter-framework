import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:json2yaml/json2yaml.dart';
import 'package:path/path.dart' as path;
import 'package:recase/recase.dart';
import 'package:yaml/yaml.dart';
import 'init.dart' as init;
import 'generate.dart' as generate;
import 'provider.dart' as provider;

void main(List<String> args) async {
  var location = Platform.script.toString();
  var isNewFlutter = location.contains(".snapshot");
  if (isNewFlutter) {
    var sp = Platform.script.toFilePath();
    var sd = sp.split(Platform.pathSeparator);
    sd.removeLast();
    var scriptDir = sd.join(Platform.pathSeparator);
    var packageConfigPath = [scriptDir, '..', '..', '..', 'package_config.json']
        .join(Platform.pathSeparator);
    // print(packageConfigPath);
    var jsonString = File(packageConfigPath).readAsStringSync();
    // print(jsonString);
    Map<String, dynamic> packages = jsonDecode(jsonString);
    var packageList = packages["packages"];
    String? ametoryUri;
    for (var package in packageList) {
      if (package["name"] == "ametory_framework") {
        ametoryUri = package["rootUri"];
        break;
      }
    }
    if (ametoryUri == null) {
      print("error uri");
      return;
    }
    if (ametoryUri.contains("../../")) {
      ametoryUri = ametoryUri.replaceFirst("../", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    if (ametoryUri.contains("file:///")) {
      ametoryUri = ametoryUri.replaceFirst("file://", "");
      ametoryUri = path.absolute(ametoryUri, "");
    }
    location = ametoryUri;
  }

  String appRootFolder = path.absolute("", "");
  var pubFile = File("$appRootFolder/pubspec.yaml");
  var doc = loadYaml(pubFile.readAsStringSync(), recover: true);
  bool isWeb = true;
  String appName = doc['name'];
  String libPath = "$location/lib";
  String commonPath = "$libPath/template/common";
  String othersPath = "$libPath/template/others";
  String commonAppPath = "$appRootFolder/lib/common";
  String routerAppPath = "$commonAppPath/route";
  String libAppPath = "$appRootFolder/lib";

  if (args.contains("--init")) {
    init.main(["--web"]);
    print("CREATE SAMPLE PAGE ...");
    var sampleContent = """
---
title: my hello page title
hide_table_of_contents: true
order: 0
icon_url: https://upload.wikimedia.org/wikipedia/commons/4/40/Home_Icon_by_Lakas.svg
icon_dark_url: https://icon-library.com/images/home-icon-png-white/home-icon-png-white-1.jpg
author: amet suramet
excerpt: Go Home
category: doc
date: 2022-09-03
description: This is only sample
---

# Hello

How are you?
""";
    File("$appRootFolder/assets/docs/home.md").writeAsStringSync(sampleContent);
    File("$location/assets/images/icons/logo.png")
        .copySync("$appRootFolder/assets/images/icons/logo.png");
    File("$location/lib/template/others/main_layout.dart.stub")
        .copySync("$appRootFolder/lib/common/main_layout.dart");

    provider.main(["collapsed_menu"]);
  }

  if (args.contains("--create")) {
    final newFeature = args[args.indexOf("--create") + 1];
    var sampleContent = """
---
title: this is new file
hide_table_of_contents: true
order: 1
icon_url: https://upload.wikimedia.org/wikipedia/commons/4/40/Home_Icon_by_Lakas.svg
icon_dark_url: https://icon-library.com/images/home-icon-png-white/home-icon-png-white-1.jpg
author: amet suramet
excerpt: new file
category: doc
date: 2022-09-03
description: new file
---

# Hello

How are you?
""";
    ReCase rcCreate = ReCase(newFeature);
    File("$appRootFolder/assets/docs/${rcCreate.snakeCase}.md")
        .writeAsStringSync(sampleContent);
  }

  var list = await dirContents(Directory("$appRootFolder/assets/docs"));
  List<String> features = [];
  for (var e in list) {
    var filename = path.basename(e.path);
    var feature = filename.replaceAll(".md", "");

    features.add("'$feature'");
  }
  String featureString = features.join(",");
  for (var e in list) {
    var filename = path.basename(e.path);
    var feature = filename.replaceAll(".md", "");
    ReCase rc = ReCase(feature);
    await generate.main([feature]);

    String screenFileName =
        "$libAppPath/feature/${rc.snakeCase}/ui/screen/${rc.snakeCase}_screen.dart";
    String screenFileTemplate =
        "$libPath/template/feature/example/ui/screen/example_doc_screen.dart.stub";
    String screenFileTemplateContent = File(screenFileTemplate)
        .readAsStringSync()
        .replaceAll("appName", appName)
        .replaceAll("example", rc.camelCase)
        .replaceAll("Example", rc.pascalCase)
        .replaceAll("EXAMPLE", rc.paramCase)
        .replaceAll("// LIST FEATURES", featureString);
    File(screenFileName).writeAsStringSync(screenFileTemplateContent);
    // File("$libAppPath/feature/${rc.paramCase}/ui/screen/${rc.paramCase}_doc_screen.dart")
    //     .deleteSync();
    print("GENERATE ${rc.paramCase} DOC DONE");
  }

  // if (args.contains("--init")) {
  String feature = "search";
  await generate.main([feature]);
  ReCase rc = ReCase(feature);

  String screenFileName =
      "$libAppPath/feature/${rc.snakeCase}/ui/screen/${rc.snakeCase}_screen.dart";
  String screenFileTemplate =
      "$libPath/template/feature/example/ui/screen/example_doc_screen.dart.stub";
  String screenFileTemplateContent = File(screenFileTemplate)
      .readAsStringSync()
      .replaceAll("appName", appName)
      .replaceAll("example", rc.camelCase)
      .replaceAll("Example", rc.pascalCase)
      .replaceAll("EXAMPLE", rc.paramCase)
      .replaceAll("// LIST FEATURES", featureString);
  File(screenFileName).writeAsStringSync(screenFileTemplateContent);
  // File("$libAppPath/feature/${rc.snakeCase}/ui/screen/${rc.snakeCase}_doc_screen.dart")
  //     .deleteSync();
  print("GENERATE ${rc.paramCase} DOC DONE");
  // }
  //
}

Future<List<FileSystemEntity>> dirContents(Directory dir) {
  var files = <FileSystemEntity>[];
  var completer = Completer<List<FileSystemEntity>>();
  var lister = dir.list(recursive: true);
  lister.listen((file) => files.add(file),
      // should also register onError
      onDone: () => completer.complete(files));
  return completer.future;
}

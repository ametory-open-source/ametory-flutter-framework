import 'package:email_validator/email_validator.dart';
import 'package:owesome_validator/owesome_validator.dart';

class Validator {
  static bool email(String text) {
    return EmailValidator.validate(text);
  }

  static bool password(String text,
      {bool camelCaseAndSpecialChar = false,
      lowerCaseAndSpecialChar = false,
      Pattern? pattern}) {
    if (camelCaseAndSpecialChar) {
      return OwesomeValidator.password(
          text, OwesomeValidator.passwordMinLen8withCamelAndSpecialChar);
    }
    if (lowerCaseAndSpecialChar) {
      return OwesomeValidator.password(
          text, OwesomeValidator.passwordMinLen8withLowerCaseAndSpecialChar);
    }
    if (pattern != null) {
      return OwesomeValidator.password(text, pattern);
    }
    return OwesomeValidator.password(
        text, OwesomeValidator.patternNameOnlyChar);
  }

  static bool idPhoneNumber(String number, {bool isFixedPhone = false}) {
    if (number.isEmpty) {
      return true;
    }
    final RegExp phoneNumberRegExp = RegExp(
      r'^(^\+62|62|^08)(\d{3,4}-?){2}\d{3,4}$',
    );
    final RegExp phoneFixedNumberRegExp = RegExp(
      r'^(^\+62|62|^0)(\d{3,4}-?){2}\d{3,4}$',
    );
    if (isFixedPhone) {
      return phoneFixedNumberRegExp.hasMatch(number);
    }
    return phoneNumberRegExp.hasMatch(number);
  }
}

// ignore_for_file: avoid_print, unnecessary_getters_setters
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';

class ApiClient {
  Dio _dio = Dio();
  String _bearerToken = "";

  set bearerToken(String value) {
    _bearerToken = value;
  }

  String get bearerToken => _bearerToken;

  ApiClient(String baseUrl, {String? bearerToken}) {
    _dio = Dio(
      BaseOptions(
        baseUrl: baseUrl,
        connectTimeout: Duration(milliseconds: 5000),
        receiveTimeout: Duration(milliseconds: 3000),
      ),
    )..interceptors.add(Logging());

    if (bearerToken != null) {
      _bearerToken = "Bearer $bearerToken";
    }
  }

  Future<Response?> getData(String endpoint,
      {Map<String, dynamic>? headers, CancelToken? cancelToken}) async {
    try {
      _dio.options.headers['content-Type'] = 'application/json';
      _dio.options.headers['authorization'] = _bearerToken;

      final response = await _dio.get(
        Uri.encodeFull(endpoint),
        options: Options(headers: headers),
        cancelToken: cancelToken,
      );
      return response;
    } on DioError catch (e) {
      throw handleException(e);
    } catch (e) {
      print("ERROR getData $e");
      throw Exception(e);
    }
  }

  Future<Response?> postData(String endpoint, dynamic data,
      {Map<String, dynamic>? headers}) async {
    try {
      _dio.options.headers['content-Type'] = 'application/json';
      _dio.options.headers['authorization'] = _bearerToken;
      final response = await _dio.post(
        Uri.encodeFull(endpoint),
        data: data,
        options: Options(headers: headers),
      );
      return response;
    } on DioError catch (e) {
      throw handleException(e);
    } catch (e) {
      print("ERROR postData $e");
      throw Exception(e);
    }
  }

  Future<Response> deleteData(String endpoint,
      {Map<String, dynamic>? headers}) async {
    try {
      _dio.options.headers['content-Type'] = 'application/json';
      _dio.options.headers['authorization'] = _bearerToken;
      final response = await _dio.get(
        Uri.encodeFull(endpoint),
        options: Options(headers: headers),
      );
      return response;
    } on DioError catch (e) {
      throw handleException(e);
    } catch (e) {
      print("ERROR postData $e");
      throw Exception(e);
    }
  }

  Future<Response?> postMultipartData(String endpoint,
      {Map<String, dynamic>? headers,
      required File file,
      required String paramName,
      required String filename}) async {
    final Map<String, MultipartFile> multipartData = {
      paramName:
          await MultipartFile.fromFile(file.uri.path, filename: filename),
    };
    final formData = FormData.fromMap(multipartData);
    return postData(endpoint, formData, headers: headers);
  }

  handleException(DioError e) {
    if (e.response != null) {
      print('Dio error!');
      print('STATUS: ${e.response?.statusCode}');
      print('DATA: ${e.response?.data}');
      print('HEADERS: ${e.response?.headers}');
    } else {
      // Error due to setting up or sending the request
      print('Error sending request!');
      print(e.message);
    }
  }
}

class Logging extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    // print('REQUEST[${options.method}] => PATH: ${options.path}');
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    // print(
    //     'RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}');
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    print(
        'ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path}');
    return super.onError(err, handler);
  }
}

// ignore_for_file: avoid_print
import 'dart:io';

void printError(String error) {
  print('[ERROR]: $error');
}

void printWarning(String error) {
  print('[WARNING]: $error');
}

String stringToType(String type) {
  switch (type) {
    case "int":
      return "int";
    case "string":
      return "String";
    case "bool":
    case "boolean":
      return "bool";
    case "double":
    case "decimal":
    case "float":
      return "double";
    case "function":
      return "Function()";
    case "map":
      return "Map<String,dynamic>";
    default:
      return type;
  }
}

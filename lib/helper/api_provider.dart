import 'api_client.dart';

abstract class ApiProvider {
  ApiClient? client;
  Future<Map<String, dynamic>?> fetchAllData(
      {int? page, int? limit, String? search, String? order, String? orderBy});
  Future<Map<String, dynamic>?> fetchSingleData({String? id});
  Future<Map<String, dynamic>?> postData({dynamic data});
  Future<Map<String, dynamic>?> putData({String? id, dynamic data});
  Future<Map<String, dynamic>?> deleteData({String? id});
}

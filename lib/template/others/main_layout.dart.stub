// ignore_for_file: depend_on_referenced_packages

import 'package:ametory_framework/models/documen_formatter.dart';
import 'package:ametory_framework/widget/responsive_widget.dart';
import 'package:ametory_ui/ametory_ui.dart' hide ResponsiveWidget;
import 'package:collection/collection.dart';
import 'package:cosmic_frontmatter/cosmic_frontmatter.dart';
import 'package:dart_markdown/dart_markdown.dart' as dmd;
import 'package:fluro/fluro.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:markdown/markdown.dart' as md;
import 'package:provider/provider.dart';
import 'package:recase/recase.dart';
import 'package:scroll_to_id/scroll_to_id.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:html2md/html2md.dart' as html2md;

import '../app/app.dart';
import 'providers/collapsed_menu_provider.dart';
import 'route/router.dart';
import 'styles/dark_theme_provider.dart';

class MainLayout extends StatefulWidget {
  final Map<String, List<String>>? params;
  final String? content;
  final Widget? logo;
  final List<String>? features;
  final GlobalKey<ScaffoldState>? scaffoldKey;
  final bool isLoading;
  final String isLoadingText;
  final String? pageTitle;
  final bool hideSidebar;
  final bool hideBottomMenu;
  final bool hideRightColumn;
  final Widget? menu;
  final Widget? endDrawer;
  final Widget? child;
  final Widget? sideWidget;
  final Widget? drawerBeforeMenu;
  final EdgeInsets? paddingContent;
  final List<MainMenuModel>? sidebarMenu;
  final List<Widget>? responsiveActions;
  final Widget? sidebarTrailing;
  final Widget? sidebarLeading;
  final Widget? drawerFooter;
  final TopBarTheme? topBarTheme;
  final SidebarTheme? sidebarTheme;

  MainLayout(
      {Key? key,
      this.content,
      this.features,
      this.logo,
      this.scaffoldKey,
      this.isLoading = false,
      this.isLoadingText = "",
      this.pageTitle,
      this.hideSidebar = false,
      this.hideBottomMenu = false,
      this.hideRightColumn = true,
      this.menu,
      this.endDrawer,
      this.child,
      this.sideWidget,
      this.drawerBeforeMenu,
      this.sidebarMenu,
      this.paddingContent,
      this.responsiveActions,
      this.sidebarTrailing,
      this.sidebarLeading,
      this.drawerFooter,
      this.topBarTheme,
      this.sidebarTheme,
      this.params})
      : super(key: key);

  @override
  State<MainLayout> createState() => _MainLayoutState();
}

class _MainLayoutState extends State<MainLayout> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController searchController = TextEditingController();
  List<DocumentFormatter> dataMenus = [];
  Map<String, List<DocumentFormatter>> groupedMenus = {};
  List<Map<String, dynamic>>? tocData;
  List<MainMenuModel> menu = [];
  DarkThemeProvider? darkThemeProvider;
  CollapsedMenuProvider? collapsedMenuProvider;
  bool collapsing = false;
  ScrollToId? scrollToId;
  final ScrollController scrollController = ScrollController();
  int activePageIndex = 0;
  int totalContent = 0;
  bool isReady = false;

  @override
  void initState() {
    initMenu();
    scrollToId = ScrollToId(scrollController: scrollController);

    scrollController.addListener(_scrollListener);
    super.initState();
  }

  void _scrollListener() {
    // print(scrollToId!.idPosition());
  }

  initMenu() async {
    for (var e in (widget.features ?? [])) {
      final content = await rootBundle.loadString("assets/docs/$e.md");
      final dataMenu = parseFrontmatter(
          content: content,
          frontmatterBuilder: (map) {
            return DocumentFormatter.fromJson(map);
          });
      dataMenu.frontmatter.feature = e;
      final data = dataMenu.frontmatter;
      data.body = dataMenu.body;
      dataMenus.add(data);
    }
    dataMenus.sort((a, b) => a.order.compareTo(b.order));
    totalContent = dataMenus.length;
    groupedMenus = groupBy(dataMenus, ((e) => e.category!));
    setState(() {});
    for (var g in groupedMenus.keys) {
      ReCase rc = ReCase(g);
      List<MainMenuModel> childMenu = [];
      for (var m in groupedMenus[g]!) {
        ReCase rc = ReCase(m.feature!);
        var route = rc.snakeCase;
        if (m.feature! == "home") route = "/";
        Widget icon = Container(
          width: 20,
        );
        if (darkThemeProvider!.darkTheme) {
          if (m.iconDarkUrl!.isNotEmpty) {
            icon = SizedBox(
              width: 20,
              height: 20,
              child: Image.network(
                "${m.iconDarkUrl}",
              ),
            );
          }
        } else {
          if (m.iconUrl!.isNotEmpty) {
            icon = SizedBox(
              width: 20,
              height: 20,
              child: Image.network(
                "${m.iconUrl}",
              ),
            );
          }
        }
        childMenu.add(MainMenuModel(
            menu: Text(rc.pascalCase),
            label: Text(
              rc.pascalCase,
              style: MyTypo.smallText.copyWith(
                  color: darkThemeProvider!.darkTheme ? Colors.white : null),
            ),
            tooltip: m.excerpt == null ? rc.pascalCase : m.excerpt!,
            icon: icon,
            onTap: () => Navigator.of(context).pushNamed(route)));
      }
      menu.add(MainMenuModel(
          menu: Text(rc.pascalCase),
          label: Text(
            rc.pascalCase,
            style: MyTypo.smallText.copyWith(
                color: darkThemeProvider!.darkTheme ? Colors.white : null),
          ),
          tooltip: "",
          children: childMenu,
          icon: null));
    }

    setState(() {});
  }

  Future<Document<DocumentFormatter>> initDoc() async {
    final content =
        await rootBundle.loadString("assets/docs/${widget.content}");
    return parseFrontmatter(
        content: content,
        frontmatterBuilder: (map) {
          final _data = DocumentFormatter.fromJson(map);
          if (!isReady) {
            SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
              setState(() {
                isReady = true;
                activePageIndex = _data.order;
              });
            });
          }
          return _data;
        });
  }

  Widget _renderSearch() {
    if (!widget.params!.containsKey("q")) return Container();
    String query = widget.params!["q"]?.first ?? "";
    final selected = dataMenus.where((e) {
      return (e.description ?? "")
              .toLowerCase()
              .contains(query.toLowerCase()) ||
          (e.title ?? "").toLowerCase().contains(query.toLowerCase()) ||
          (e.body ?? "").toLowerCase().contains(query.toLowerCase()) ||
          (e.title ?? "").toLowerCase().contains(query.toLowerCase()) ||
          (e.excerpt ?? "").toLowerCase().contains(query.toLowerCase());
    });
    return Container(
      padding: const EdgeInsets.all(20),
      height: double.infinity,
      width: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Result for : $query",
              style: const TextStyle().copyWith(fontSize: 18),
            ),
            const SizedBox(
              height: 40,
            ),
            if (selected.isEmpty) const Text("No contents found"),
            ...selected
                .map((e) => InkWell(
                      hoverColor: Colors.transparent,
                      onTap: () {
                        Application.router.navigateTo(
                            context, "/${e.feature == 'home' ? '' : e.feature}",
                            transition: TransitionType.fadeIn);
                      },
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  e.title!,
                                  style: const TextStyle().copyWith(
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold),
                                ),
                                if (e.description != null)
                                  const SizedBox(
                                    height: 10,
                                  ),
                                if (e.description != null)
                                  Text(
                                    e.description!,
                                    style: const TextStyle().copyWith(),
                                  ),
                                const SizedBox(
                                  height: 20,
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          ),
                          const Icon(
                            Icons.chevron_right_rounded,
                            color: Colors.grey,
                            size: 50,
                          )
                        ],
                      ),
                    ))
                .toList()
          ],
        ),
      ),
    );
  }

  Widget _renderToc() {
    if (tocData == null) return Container();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: tocData!
          .map((e) => InkWell(
              hoverColor: Colors.transparent,
              onTap: () {
                String index = e["index"];
                scrollToId!.animateTo(index,
                    duration: const Duration(milliseconds: 500),
                    curve: Curves.ease);
              },
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text(
                  "${e['label']}",
                  style: MyTypo.linkText
                      .copyWith(fontWeight: FontWeight.w600, fontSize: 14),
                ),
              )))
          .toList(),
    );
  }

  parseToMap(List<dmd.Node> data) {
    if (tocData != null) return;
    List<Map<String, dynamic>> _data = [];
    for (var i = 0; i < data.length; i++) {
      final dataNode = data[i].toMap();
      // print(dataNode);
      if (dataNode["type"] != "headline") continue;
      if ((dataNode["children"] as List).isEmpty) continue;
      _data.add(
          {"index": i.toString(), "label": dataNode["children"].first["text"]});
    }

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        tocData = _data;
      });
    });
    // print(_data);
  }

  Widget _renderNavigation(int index, int total) {
    final menu = dataMenus[index];
    DocumentFormatter? prev;
    DocumentFormatter? next;
    if (index > 0) {
      prev = dataMenus[index - 1];
    }
    if (index < total - 1) {
      next = dataMenus[index + 1];
    }
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _renderNavigationButton(prev, false),
          _renderNavigationButton(next, true),
        ],
      ),
    );
  }

  Widget _renderNavigationButton(DocumentFormatter? data, bool isNext) {
    darkThemeProvider = context.watch<DarkThemeProvider>();
    if (data == null) return Container();
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border.all(
              color: darkThemeProvider!.darkTheme
                  ? Colors.white12
                  : Colors.black.withOpacity(0.2),
              width: 0.5),
          borderRadius: BorderRadius.circular(5)),
      child: InkWell(
        hoverColor: Colors.transparent,
        onTap: () {
          Application.router.navigateTo(
              context, "/${data.feature == 'home' ? '' : data.feature}",
              transition: TransitionType.fadeIn);
        },
        child: Row(
          children: [
            if (!isNext)
              Icon(Icons.chevron_left,
                  color: darkThemeProvider!.darkTheme
                      ? Colors.white12
                      : Colors.black.withOpacity(0.2)),
            if (!isNext) hspace(10),
            Column(
              crossAxisAlignment:
                  !isNext ? CrossAxisAlignment.start : CrossAxisAlignment.end,
              children: [
                Text(
                  data.title!,
                  style: MyTypo.linkText
                      .copyWith(fontWeight: FontWeight.w600, fontSize: 14),
                ),
                Text(
                  data.excerpt!,
                ),
              ],
            ),
            if (isNext) hspace(10),
            if (isNext)
              Icon(Icons.chevron_right,
                  color: darkThemeProvider!.darkTheme
                      ? Colors.white12
                      : Colors.black.withOpacity(0.2)),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    darkThemeProvider = context.watch<DarkThemeProvider>();
    collapsedMenuProvider = context.watch<CollapsedMenuProvider>();
    return PageSideBar(
      scaffoldKey: _scaffoldKey,
      onCollapsedTap: ((collapsed, collapsing) {
        collapsing = collapsing;
        setState(() {});
        Provider.of<CollapsedMenuProvider>(context, listen: false).value =
            collapsed;
        // print(collapsed);
      }),
      topBarTheme: widget.topBarTheme ??
          TopBarTheme(
              elevation: 0.5,
              color: darkThemeProvider!.darkTheme ? Colors.black87 : null),
      sidebarTheme: widget.sidebarTheme ??
          SidebarTheme(
              elevation: 0.5,
              color: darkThemeProvider!.darkTheme ? Colors.black87 : null),
      sideBarMenu: menu,
      responsiveActions: widget.responsiveActions ??
          [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: InkWell(
                  onTap: () {
                    Provider.of<DarkThemeProvider>(context, listen: false)
                        .darkTheme = !darkThemeProvider!.darkTheme;
                  },
                  child: Icon(
                    Icons.brightness_high,
                    color: darkThemeProvider!.darkTheme ? null : Colors.black12,
                  )),
            ),
            IconButton(
              onPressed: () {
                _scaffoldKey.currentState?.openEndDrawer();
              },
              icon: Icon(
                Icons.list_alt,
                color: darkThemeProvider!.darkTheme
                    ? Colors.white
                    : Colors.black12,
              ),
            )
          ],
      endDrawer: Padding(
        padding: const EdgeInsets.all(20.0),
        child: widget.endDrawer ?? _renderToc(),
      ),
      paddingContent: widget.paddingContent,
      isCollapsed: collapsedMenuProvider!.value,
      topBarContent: Input(
        height: 30,
        width: 400,
        hintText: "Search ....",
        padding: const EdgeInsets.fromLTRB(10, 5, 10, 15),
        backgroundColor: darkThemeProvider!.darkTheme ? Colors.white12 : null,
        textFieldStyle: TextStyle(
            color: !darkThemeProvider!.darkTheme ? Colors.black87 : null,
            fontSize: 10),
        hintStyle: TextStyle(
            color: !darkThemeProvider!.darkTheme ? Colors.black87 : null,
            fontSize: 10),
        onSubmitted: ((text) {
          Application.router.navigateTo(context, "/search?q=$text",
              transition: TransitionType.fadeIn);
        }),
      ),
      topBarTrailing: InkWell(
          onTap: () {
            Provider.of<DarkThemeProvider>(context, listen: false).darkTheme =
                !darkThemeProvider!.darkTheme;
          },
          child: const Icon(Icons.brightness_high)),
      showLabelCollapsed: true,
      pageTitle: widget.pageTitle,
      hideSidebar: widget.hideSidebar,
      responsiveLeading: IconButton(
        onPressed: () {
          _scaffoldKey.currentState?.openDrawer();
        },
        icon: Icon(
          Icons.menu,
          color: darkThemeProvider!.darkTheme ? Colors.white : Colors.black12,
        ),
      ),
      drawerBeforeMenu: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20.0),
        child: Input(
          height: 30,
          width: 400,
          hintText: "Search ....",
          padding: const EdgeInsets.fromLTRB(10, 5, 10, 15),
          backgroundColor: darkThemeProvider!.darkTheme ? Colors.white12 : null,
          textFieldStyle: TextStyle(
              color: !darkThemeProvider!.darkTheme ? Colors.black87 : null,
              fontSize: 10),
          hintStyle: TextStyle(
              color: !darkThemeProvider!.darkTheme ? Colors.black87 : null,
              fontSize: 10),
          onSubmitted: ((text) {
            Application.router.navigateTo(context, "/search?q=$text",
                transition: TransitionType.fadeIn);
          }),
        ),
      ),
      topBarLeading: widget.logo ??
          SizedBox(
            width: !ResponsiveWidget.isLargeScreen(context) ? 30 : 100,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                    height: 30,
                    child: Image.asset('assets/images/icons/logo.png')),
              ],
            ),
          ),
      child: ModalRoute.of(context)!.settings.name!.contains(Routes.search)
          ? _renderSearch()
          : FutureBuilder<Document<DocumentFormatter>>(
              future: initDoc(),
              builder: (BuildContext context,
                  AsyncSnapshot<Document<DocumentFormatter>> snapshot) {
                if (snapshot.hasData) {
                  final parsed = dmd.Document().parseLines(snapshot.data!.body);
                  parseToMap(parsed);
                  activePageIndex = snapshot.data!.frontmatter.order;
                  totalContent = dataMenus.length;
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.fromLTRB(20, 10, 20, 0),
                          width: double.infinity,
                          height: double.infinity,
                          child: InteractiveScrollViewer(
                            scrollToId: scrollToId,
                            children: [
                              ScrollContent(
                                child: Container(
                                  padding: const EdgeInsets.only(bottom: 20.0),
                                  width: double.infinity,
                                  child: Text(
                                    "${snapshot.data!.frontmatter.title}",
                                    style: MyTypo.heading1.copyWith(
                                        fontWeight: FontWeight.w600,
                                        color: darkThemeProvider!.darkTheme
                                            ? Colors.white
                                            : null),
                                  ),
                                ),
                                id: 'title',
                              ),

                              ScrollContent(
                                child: Container(
                                  padding: const EdgeInsets.only(bottom: 20.0),
                                  width: double.infinity,
                                  child: Text(
                                    "${snapshot.data!.frontmatter.description}",
                                    style: MyTypo.bodyText1.copyWith(
                                        color: darkThemeProvider!.darkTheme
                                            ? Colors.white
                                            : null),
                                  ),
                                ),
                                id: 'description',
                              ),

                              // MarkdownBody(
                              //   data: snapshot.data!.body,
                              //   onTapLink: (text, url, title) {
                              //     launchUrlString(url!);
                              //   },
                              //   selectable: true,
                              //   extensionSet: md.ExtensionSet(
                              //     md.ExtensionSet.gitHubFlavored
                              //         .blockSyntaxes,
                              //     [
                              //       md.EmojiSyntax(),
                              //       ...md.ExtensionSet.gitHubFlavored
                              //           .inlineSyntaxes
                              //     ],
                              //   ),
                              // )

                              ...parsed.map((e) => ScrollContent(
                                    id: parsed.indexOf(e).toString(),
                                    child: Container(
                                      padding:
                                          const EdgeInsets.only(bottom: 10.0),
                                      width: double.infinity,
                                      child: MarkdownBody(
                                        data: html2md
                                            .convert(dmd.renderToHtml([e])),
                                        onTapLink: (text, url, title) {
                                          launchUrlString(url!);
                                        },
                                        selectable: true,
                                        extensionSet: md.ExtensionSet(
                                          md.ExtensionSet.gitHubFlavored
                                              .blockSyntaxes,
                                          [
                                            md.EmojiSyntax(),
                                            ...md.ExtensionSet.gitHubFlavored
                                                .inlineSyntaxes
                                          ],
                                        ),
                                      ),
                                    ),
                                  )),

                              ScrollContent(
                                child: _renderNavigation(
                                    activePageIndex, totalContent),
                                id: 'pagination',
                              ),
                            ],
                          ),
                        ),
                      ),
                      if (ResponsiveWidget.isLargeScreen(context))
                        Container(
                          padding: const EdgeInsets.all(10),
                          width: 300,
                          child: _renderToc(),
                        )
                    ],
                  );
                }
                return Container();
              }),
    );
  }
}

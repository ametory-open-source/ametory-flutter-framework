class DocumentFormatter {
  String? feature;
  String? title;
  String? author;
  String? excerpt;
  String? category;
  String? iconUrl;
  String? iconDarkUrl;
  DateTime? date;
  String? description;
  String? body;
  late int order;
  late bool hideTableContent;

  DocumentFormatter(
      {this.title,
      this.author,
      this.excerpt,
      this.category,
      this.date,
      this.order = 1,
      this.iconUrl,
      this.iconDarkUrl,
      this.description,
      this.hideTableContent = false});

  DocumentFormatter.fromJson(Map<String, dynamic> json) {
    feature = json['feature'] ?? "";
    title = json['title'];
    author = json['author'];
    excerpt = json['excerpt'];
    iconUrl = json['icon_url'] ?? "";
    iconDarkUrl = json['icon_dark_url'] ?? "";
    category = json['category'];
    description = json['description'];
    body = json['body'] ?? "";
    order = json['order'] ?? 1;
    hideTableContent = json['hide_table_of_contents'];
    if (json['date'] != null) date = DateTime.parse(json['date']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['feature'] = feature ?? "";
    data['title'] = title;
    data['author'] = author;
    data['excerpt'] = excerpt;
    data['icon_url'] = iconUrl ?? "";
    data['icon_dark_url'] = iconDarkUrl ?? "";
    data['category'] = category;
    data['description'] = description;
    data['body'] = body ?? "";
    data['order'] = order;
    data['hide_table_of_contents'] = hideTableContent;
    if (date != null) {
      data['date'] = date!.toIso8601String();
    }
    return data;
  }
}

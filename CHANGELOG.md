## 0.0.1

* add init function
* add generate feature function

## 0.0.2

* fixing bug uri contains file:///
## 0.0.3

* fixing bug router_generator.stub
## 0.0.4

* add dark theme provider
* add page transition
* fix flutter bloc version
* fix bloc version
## 0.0.5

* add web compability
* add fluro router
* update flutter bloc & bloc package to latest version

## 0.0.6

* add provider command

## 0.0.7

* fixing bug: delete simple bloc delegate
## 0.0.8

* fixing bug: import router_generator for web mode
## 0.0.9

* add helper & utils

## 0.0.10
* delete error utils function
## 0.0.11
* add api client
* add model generate
* add widget generate

## 0.0.12
* add model in feature generate

## 0.0.13
* add page in feature generate
## 0.0.14
* fixing bug generate home page
## 0.0.15
* fixing bloc stub
## 0.0.16
* add doc generator
## 0.0.17
* update readme
## 0.0.18
* update ametory ui version
## 0.0.19
* update ametory ui version
## 0.0.20
* fixing generate
## 0.0.21
* update ametory ui version

## 0.0.22
* update ametory ui version
## 0.0.23
* Fix bug delete doc template
## 0.0.24
* Link handle 
## 0.0.25
* Fix bug link handler
## 0.0.26
* Fix doc init
## 0.0.27
* Add doc scroll to index
## 0.0.28
* Add pagination
## 0.0.29
* update ametory ui
## 0.0.30
* update ametory ui
## 0.0.31
* add admin generator
## 0.0.32
* add user data
## 0.0.33
* add dashboard template
## 0.0.34
* Fix bug main layout
## 0.0.35
* update main_layout & dashbord template
## 0.0.36
* update main_layout & dashbord template
## 0.0.37
* update main_layout & dashbord template
## 0.0.39
* add collapsible group & refactor forbidden access page
## 0.0.40
* Update ametory ui version
## 0.0.41
* Update ametory ui version
## 0.0.43
* Update ametory ui version
## 0.0.44
* Update ametory ui version
## 0.0.45
* Update ametory ui version
## 0.0.46
* Update ametory ui version
## 0.0.47
* Update ametory ui version
## 0.0.48
* Update ametory ui version
## 0.0.49
* Update ametory ui version
## 0.0.50
* Update ametory ui version
## 0.0.51
* Update ametory ui version
## 0.0.52
* Update ametory ui version
## 0.0.53
* fix bug header chat tap
## 0.0.54
* fix bug header chat tap
## 0.0.55
* fix bug header chat tap
## 0.0.56
add day model calendar
## 0.0.57
add blog template
## 0.0.58
fixing blog template
## 0.0.59
fixing calendar
## 0.0.60
add event model
## 0.0.61
update ametory ui
## 0.0.62
update ametory ui
## 0.0.63
update ametory ui
## 0.0.64
update ametory ui
## 0.0.65
update ametory ui
## 0.0.66
update ametory ui
## 0.0.67
update ametory ui
## 0.0.68
update ametory ui
## 0.0.69
update ametory ui
## 0.0.70
update ametory ui
## 0.0.71
update ametory ui
## 0.0.72
update ametory ui
## 0.0.73
update ametory ui
## 0.0.74
update ametory ui
## 0.0.75
update ametory ui
## 0.0.76
add dep override
## 0.0.77
update ametory ui / input max letter
## 0.0.78
update markdown viewer
## 0.0.79
update ametory ui 
## 0.0.80
update ametory ui 
## 0.0.81
fixing generate route
## 0.0.82
update ametory ui 
## 0.0.83
update ametory ui 
## 0.0.84
update ametory ui 
## 0.0.86
Update flutter sdk
## 0.0.85
update ametory ui 
## 0.0.87
update ametory ui 
## 0.0.88
update ametory ui 
## 0.0.89
update ametory ui 
## 0.0.90
update dependencies
## 0.0.91
update dependencies